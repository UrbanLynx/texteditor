﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using TextEditor.ViewModel;
using Xceed.Wpf.Toolkit;
//using Brush = System.Drawing.Brush;

namespace TextEditor.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BeginChangeTextInRichBox()
        {
            RichTextBox1.TextChanged -= RichTextBox1BoxTextBox1Changed;
            int pos = RichTextBox1.CaretPosition.GetOffsetToPosition(RichTextBox1.Document.ContentEnd);

            

            RichTextBox1.CaretPosition = RichTextBox1.Document.ContentEnd.GetPositionAtOffset(-pos);
            RichTextBox1.TextChanged += RichTextBox1BoxTextBox1Changed;
        }
        private void RichTextBox1BoxTextBox1Changed(object sender, TextChangedEventArgs e)
        {
            RichTextBox1.TextChanged -= RichTextBox1BoxTextBox1Changed;
            int pos = RichTextBox1.CaretPosition.GetOffsetToPosition(RichTextBox1.Document.ContentEnd);

            MainViewModel.Instance.TextChange(RichTextBox1.Document);

            RichTextBox1.CaretPosition = RichTextBox1.Document.ContentEnd.GetPositionAtOffset(-pos);
            RichTextBox1.TextChanged += RichTextBox1BoxTextBox1Changed;
            
        }

        private void UndoItem_OnClick(object sender, RoutedEventArgs e)
        {
            RichTextBox1.TextChanged -= RichTextBox1BoxTextBox1Changed;
            int pos = RichTextBox1.CaretPosition.GetOffsetToPosition(RichTextBox1.Document.ContentEnd);

            RichTextBox1.Document = MainViewModel.Instance.Undo();

            RichTextBox1.CaretPosition = RichTextBox1.Document.ContentEnd.GetPositionAtOffset(-pos);
            RichTextBox1.TextChanged += RichTextBox1BoxTextBox1Changed;
        }

        private void RedoItem_OnClick(object sender, RoutedEventArgs e)
        {
            RichTextBox1.TextChanged -= RichTextBox1BoxTextBox1Changed;
            int pos = RichTextBox1.CaretPosition.GetOffsetToPosition(RichTextBox1.Document.ContentEnd);

            RichTextBox1.Document = MainViewModel.Instance.Redo();

            RichTextBox1.CaretPosition = RichTextBox1.Document.ContentEnd.GetPositionAtOffset(-pos);
            RichTextBox1.TextChanged += RichTextBox1BoxTextBox1Changed;
        }

        private void OpenItem_OnClick(object sender, RoutedEventArgs e)
        {
            var text = MainViewModel.Instance.OpenFile();
            if (text != null)
            {
                RichTextBox1.Document.Blocks.Clear();
                RichTextBox1.Document.Blocks.Add(new Paragraph(new Run(text)));
            }
        }

        private void NewItem_OnClick(object sender, RoutedEventArgs e)
        {
            MainViewModel.Instance.NewFile();
            RichTextBox1.Document.Blocks.Clear();
            RichTextBox1.Document.Blocks.Add(new Paragraph());
        }

        private void SaveItem_OnClick(object sender, RoutedEventArgs e)
        {
            MainViewModel.Instance.SaveFile();
        }

        private void SaveAsItem_OnClick(object sender, RoutedEventArgs e)
        {
            MainViewModel.Instance.SaveFileAs();
        }

        
    }
}
