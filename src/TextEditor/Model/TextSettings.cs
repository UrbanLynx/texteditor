﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TextEditor.Model
{
    class TextSettings
    {
        public TextSettings()
        {
            AssociationConfig = new AssociationConfig();
            Text = "";
            FileType = new List<string>();
        }

        public AssociationConfig AssociationConfig { get; set; }
        public string Text { get; set; }
        public List<string> FileType { get; set; }
    }
}
