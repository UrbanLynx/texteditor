﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace TextEditor.Model
{
    class Journal
    {
        private Stack<FlowDocument> _undo = new Stack<FlowDocument>();
        private Stack<FlowDocument> _redo = new Stack<FlowDocument>();

        public void Log(FlowDocument document)
        {
            /*if (_undo.Count > 0)
            {
                var document1 = _undo.Peek();
                var text = new TextRange(document1.ContentStart, document1.ContentEnd).Text;
            }*/
            var logdoc = new FlowDocument();
            AddDocument(document,logdoc);

            if(_redo.Count > 0)
                _redo.Clear();
            _undo.Push(logdoc);
        }

        public FlowDocument Undo()
        {
            var peek = _undo.Pop();
            _redo.Push(peek);
            return _undo.Peek();
        }

        public FlowDocument Redo()
        {
            var peek = _redo.Pop();
            _redo.Push(peek);
            return peek;
        }

        public static void AddDocument(FlowDocument from, FlowDocument to)
        {
            TextRange range = new TextRange(from.ContentStart, from.ContentEnd);
            MemoryStream stream = new MemoryStream();
            System.Windows.Markup.XamlWriter.Save(range, stream);
            range.Save(stream, DataFormats.XamlPackage);
            TextRange range2 = new TextRange(to.ContentEnd, to.ContentEnd);
            range2.Load(stream, DataFormats.XamlPackage);
        }
        
        public static void AddBlock(Block from, FlowDocument to)
        {
            if (from != null)
            {
                TextRange range = new TextRange(from.ContentStart, from.ContentEnd);
                MemoryStream stream = new MemoryStream();
                System.Windows.Markup.XamlWriter.Save(range, stream);
                range.Save(stream, DataFormats.XamlPackage);
                TextRange textRange2 = new TextRange(to.ContentEnd, to.ContentEnd);
                textRange2.Load(stream, DataFormats.XamlPackage);
            }
        }
    }
}
