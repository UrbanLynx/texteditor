﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace TextEditor.Model
{
    class AssociationConfig
    {
        public AssociationConfig()
        {
            WordProperties = new Dictionary<string, Properties>();
        }
        public struct Properties
        {
            public Color Color { get; set; }
        }
        public Dictionary<string, Properties> WordProperties { get; set; }
    }
}
