﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Xml.Linq;
using nJupiter.Configuration;

namespace TextEditor.Model
{
    class FileManager
    {
        private string _configFileName = @"E:\Study\Software Design\3 Lab\TextEditor\src\TextEditor\System.config";
        public TextSettings LoadFrom(string filename)
        {
            var text = new TextSettings();
            text.Text = LoadText(filename);
            text.AssociationConfig = LoadConfig(Path.GetExtension(filename), _configFileName);
            FileName = filename;
            return text;
        }

        private string LoadText(string filename)
        {
            string content = "";
            using (var reader = new StreamReader(filename))
            {
                content = reader.ReadToEnd();
            }
            return content;
        }

        public void Save(string text)
        {
            SaveTo(FileName,text);
        }
        public void SaveTo(string filename, string text)
        {
            using (var writer = new StreamWriter(filename))
            {
                writer.Write(text);
                /*content = reader.ReadToEnd();*/
            }
            FileName = filename;
        }

        private AssociationConfig LoadConfig(string filetype)
        {
            //TODO: посмотреть, приходит тип файла с точкой или без
            var textConfig = new AssociationConfig();

            IConfigRepository configRepository = ConfigRepository.Instance;
            IConfig config = configRepository.GetConfig();
            string language = config.GetAttribute("fileAssociation/fileTypes/file[@type='" + filetype + "']", "language");
            IConfig formatConfig = config.GetConfigSection("fileAssociation/formats/formatting[@languge='" + language + "']");

            var xElement = XElement.Parse(formatConfig.ConfigXml.OuterXml);

            foreach (var el in xElement.Elements())
            {
                var word = el.Attribute("word").Value;
                var properties = new AssociationConfig.Properties();
                properties.Color = (Color)ColorConverter.ConvertFromString(el.Attribute("color").Value.ToString());
                textConfig.WordProperties.Add(word, properties);
            }
            return textConfig;
        }

        private AssociationConfig LoadConfig(string filetype, string configFile)
        {
            try
            {
                var xDoc = new System.Xml.XmlDocument();
                var stream = new FileStream(configFile, FileMode.Open);

                xDoc.Load(stream);

                var language =
                    xDoc.SelectSingleNode("/configuration/fileAssociation/fileTypes/file[@type='" + filetype + "']")
                        .Attributes["language"].Value;

                var formattingXmlNode =
                    xDoc.SelectSingleNode("/configuration/fileAssociation/formats/formatting[@language='" + language +
                                          "']");
                
                var textConfig = new AssociationConfig();
                var xElement = XElement.Parse(formattingXmlNode.OuterXml);

                foreach (var el in xElement.Elements())
                {
                    var word = el.Attribute("word").Value;
                    var properties = new AssociationConfig.Properties();
                    properties.Color = (Color) ColorConverter.ConvertFromString(el.Attribute("color").Value.ToString());
                    textConfig.WordProperties.Add(word, properties);
                }
                return textConfig;
                
            }
            catch (Exception)
            {
                return null;
            }

        }

        private NameValueCollection GetNameValueCollectionSection(string section, string filePath)
        {
            string file = filePath;
            System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
            NameValueCollection nameValueColl = new NameValueCollection();

            System.Configuration.ExeConfigurationFileMap map = new ExeConfigurationFileMap();
            map.ExeConfigFilename = file;
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
            string xml = config.GetSection(section).SectionInformation.GetRawXml();
            xDoc.LoadXml(xml);

            System.Xml.XmlNode xList = xDoc.ChildNodes[0];
            foreach (System.Xml.XmlNode xNodo in xList)
            {
                nameValueColl.Add(xNodo.Attributes[0].Value, xNodo.Attributes[1].Value);

            }

            return nameValueColl;
        }

        public string FileName { get; private set; }
    }
}
