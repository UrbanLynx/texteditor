﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using TextEditor.Model;

namespace TextEditor.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Members
        
        private static volatile MainViewModel _instance;
        private static object _syncRoot = new Object();

        private TextSettings _textSettings = new TextSettings();
        private FileManager _fileManager = new FileManager();
        private Journal _journal = new Journal();
        private string _text;

        #endregion

        #region Constructors
        private MainViewModel() { }

        public static MainViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new MainViewModel();
                    }
                    MainViewModel.Instance.Creator();
                }

                return _instance;
            }
        }
        #endregion

        #region Methods

        private void Creator()
        {
        }

        public bool IsWordColored(string word)
        {
            return _textSettings.AssociationConfig.WordProperties.ContainsKey(word);
        }

        public Color GetColorOfWord(string word)
        {
            if (IsWordColored(word)) // посмотреть правильно ли извлекается значение Color?
                return _textSettings.AssociationConfig.WordProperties[word].Color;
            return Colors.Black;
        }

        public bool TextChange(FlowDocument document)
        {
            Text = new TextRange(document.ContentStart, document.ContentEnd).Text;
            _journal.Log(document);

            foreach (Paragraph paragraph in document.Blocks.ToList())
            {
                string text = new TextRange(paragraph.ContentStart, paragraph.ContentEnd).Text;

                paragraph.Inlines.Clear();
                var numberOfLines = text.Split(new string[] {"\n\r"}, StringSplitOptions.None).Length;
                var currentLine = 0;

                using (StringReader reader = new StringReader(text))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        currentLine++;
                        string[] wordSplit = line.Split(new char[] { ' ' });
                        int count = 1;

                        foreach (string word in wordSplit)
                        {
                            string beginWord = "", middleWord = word, endWord = "";

                            //edit begin
                            var firstChar = middleWord.FirstOrDefault();
                            while (firstChar == '\n' || firstChar == '\r' || firstChar == '\t')
                            {
                                beginWord += firstChar;
                                middleWord = middleWord.Remove(0, 1);
                                firstChar = middleWord.FirstOrDefault();
                            }

                            //edit end
                            var endChar = middleWord.LastOrDefault();
                            while (endChar == '\n' || endChar == '\r' || endChar == '\t' || endChar == ';' ||
                                   endChar == '.')
                            {
                                endWord = endChar + endWord;
                                middleWord = middleWord.Remove(middleWord.Length - 1, 1);
                                endChar = middleWord.LastOrDefault();
                            }

                            Run run = new Run(middleWord);
                            var color = MainViewModel.Instance.GetColorOfWord(middleWord);
                            run.Foreground = new SolidColorBrush(color);

                            paragraph.Inlines.Add(new Run(beginWord));
                            paragraph.Inlines.Add(run);
                            paragraph.Inlines.Add(new Run(endWord));


                            if (count++ != wordSplit.Length)
                            {
                                paragraph.Inlines.Add(" ");
                            }

                        }
                        if(currentLine<numberOfLines)
                            paragraph.Inlines.Add("\r\n");
                    }
                }
            }
            return true;
        }
        public string OpenFile()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog();
            var result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;
                _textSettings = _fileManager.LoadFrom(filename);
                return _textSettings.Text;
            }
            return null;
        }

        public bool SaveFileAs()
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            //dlg.FileName = _fileManager.FileName; // Default file name
            //dlg.DefaultExt = ".text"; // Default file extension
            //dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension
            
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;
                _fileManager.SaveTo(filename,Text);
                return true;
            }
            return false;
        }

        public void SaveFile()
        {
            if (_fileManager.FileName == null)
                SaveFileAs();
            else
                _fileManager.Save(Text);
        }

        public void NewFile()
        {
            Text = "";
            if (SaveFileAs())
            {
                _textSettings = _fileManager.LoadFrom(_fileManager.FileName);
            }
            
        }
        #endregion

        #region Properties

        

        public string Text
        {
            get { return _text; }
            set { _text = value; OnPropertyChanged("Text"); }
        }

        public 
        #endregion

        #region Commands

        #region OpenCom

        void OpenComExecute()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog();
            var result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;
                _textSettings = _fileManager.LoadFrom(filename);

            }
        }

        bool CanOpenComExecute()
        {
            return true;
        }
        public ICommand OpenCom
        {
            get { return new RelayCommand(param => this.OpenComExecute(), param => this.CanOpenComExecute()); }
        }

        #endregion
        /*#region RemoveVM
        void RemoveVMExecute()
        {
            if (SelectedItem is PostVM)
            {
                AreaVM ParentArea = (SelectedItem as PostVM).Area;
                (SelectedItem as PostVM).Remove();
                SaveChanges();
                ParentArea.OnPropertyChanged("Posts");
                return;
            }
            if (SelectedItem is ServerVM)
            {
                PostVM ParentPost = (SelectedItem as ServerVM).Post;
                (SelectedItem as ServerVM).Remove();
                SaveChanges();
                ParentPost.OnPropertyChanged("Servers");
                return;
            }
            if (SelectedItem is CameraVM)
            {
                ServerVM ParentServer = (SelectedItem as CameraVM).Server;
                (SelectedItem as CameraVM).Remove();
                SaveChanges();
                ParentServer.OnPropertyChanged("Cameras");
                return;
            }
        }

        bool CanRemoveVMExecute()
        {
            if (SelectedItem is PostVM || SelectedItem is ServerVM || SelectedItem is CameraVM)
                return true;
            return false;
        }

        public ICommand RemoveVM
        {
            get { return new RelayCommand(param => this.RemoveVMExecute(), param => this.CanRemoveVMExecute()); }
        }
        #endregion
*/

        
        #endregion

        public FlowDocument Undo()
        {
            return _journal.Undo();
        }

        public FlowDocument Redo()
        {
            return _journal.Redo();
        }
    }
}
